﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace lesson4
{
    class MyRectangle
    {
        int _width = 0;
        int _height = 0;
        public MyRectangle(int w, int h)
        {
            _width = w;
            _height = h;
        
        }

        public int GetArea()
        {

            return (_width * _height);
        }
        public int GetPerimeter()
        {
            return ((2 * _width) + (2 * _height));
        } 
  
        
    }

    

}
